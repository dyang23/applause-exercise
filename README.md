
## How to run code

1. Initiate with file path of four datasets 
2. Get Criterias from user. Input format has to be the same as original dataset, and there should be ', ' in between if multiple criterias are needed
3. Enter stop to exit the program.

---

## Logic behind the code

1. Keep Tester Id with criteria Countries (from testers dataset)
2. Keep Device Id with criteria Devices (from decises dataset)
3. Keep bugs in given criterias (from bugs dataset)
4. Apply aggregation function to bugs dataset to see how many bugs each of the user has filed 
5. Merge the result dataset and testers dataset to get the name of testers and their counts of bugs filed
